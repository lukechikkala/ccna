# CCNA 200-301

## Network Characteristics
* Topology
* Speed
* Cost
* Security
* Availability
* Scalability
* Reliability

# OSI Model
* **O**pen **S**ystem **I**nterconnect<br>
* **I**netenational **S**tandardization **O**rganization
* "OSI Reference Model" is only a conceptual model.. Hence the tag "Reference Model".

```
7. Application
6. Presentation
5. Session
4. Transport
3. Data-Link
2. Network
1. Physical
```

# TCP/IP Suite
* A protocol stack that is used today.
* 1960
* US Department of Defence [DoD]
* Advanced Research Projects Agency [ARPA]
* Also a layered structure but does not use all the layers of OSI model.

<table>
	<thead>
		<tr>
			<th>OSI Model</th>
			<th>TCP/IP Stack</th>
			<th>PDU</th>
			<th>Definition</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Application</td>
			<td rowspan=3>Application</td>
			<td rowspan=3>Represets data users, encodes abd controls teh dialogue.</td>
		</tr>
		<tr>
			<td>Presentation</td>
		</tr>
		<tr>
			<td>Session</td>
		</tr>
		<tr>
			<td>Transport</td>
			<td>Transport</td>
			<td>Supports communication between end devices across a diverse network</td>
		</tr>
		<tr>
			<td>Network</td>
			<td>Internet</td>
			<td>Provides logical addressing & determines the best path through the network</td>
		</tr>
		<tr>
			<td>Data Link</td>
			<td rowspan=2>Network Access</td>
			<td rowspan=2>Controls the hardware devices and media that make up the network</td>
		</tr>
		<tr>
			<td>Physical</td>
		</tr>
	</tbody>
</table>

* **P**rotocol **D**ata **U**nit are the elements exchanged between devices.

The units of Application Layer are calles **Data**<br>
Transport Layer &nbsp;---> **Session**<br>
Internet Layer &nbsp;&nbsp;&nbsp;&nbsp;---> **Packet**<br>
Network Access ---> **Frame**<br>

## Upper OSI Layers
### 7 - Application Layer
* Provides network services to teh application.
* Does not provide services to other OSI layers.
* Establishes availability of hosts.
* Synchronizes & establishes agreement on procedures for error recovery & control of data integrity [alteration & corruption of data].

### 6 - Presentation Layer
* Ensures that the data is readable by the receiver.
* Translates multiple different formats (eg. computers with different encogin schemes)

### 5 - Session Layer
* Establishes manages & terminates session b/w two hosts.
* Synchronizes dialog b/w layer-6 of each host and manages their data exchange.
* Eg.: Webserver with many hosts; Layer-5 manages the session among all the users.
* Offers:
	* Efficient data transfer
	* Class of Service
	* Exception Reporting from upper layers.

## Lower OSI Layers
### 4 - Transport Layer
* Responsible for:
	* TCP or UDP
	* Port Number
* Defines services to be segmented, transfered & reassembled between the hosts.
* Breaks large files into smaller segments to avoid transmission problems.

### 3 - Network Layer
* IP addresses are the important information.
* Routers are Layer-3 devices.
* Provides connectivity & **path selection** among hosts.
* Manages connectivity of hosts by providing logical addressing [IP Addresses].

### 2 - Data-Link Layer
* Layer-2 Addresses are important information.
	* For Ethernet, Layer-2 Address is MAC Addresses
	* For legacy frame relay. Layer-2 address would be DLCI numbering.
* Switches are Layer-2 devices.
* Defines how data is formatted for transmission and how access to phycial media is controlled.
* Typically includes error detection and correction to ensure reliable delivery of data.

### 1 - Physical Layer
* Cables & Air
* Enables bit transmission among hosts.
* Defines specs needed for activating, maintaining & deactivating physical links.
* Eg.: Voltage levels, Physical data rates, max. transmission distances, physical connecctors.

# Section - 4
## Short History
* 1984: Cisco was a Router manufacturing company.
* 1993: Cisco acquired **Crescendo** & produced Catalyst Switches.
	* CatOS was the first Cisco's OS.
* 1995: Cisco aqruired Network Translation's PIX Firewall.
	* Cisco IOS was later developed.

## Operating Systems:

* **NX-OS**: Cisco Nexus * MDS data center switch products.
* **IOS-XR**: Service Provider routers (NCS, CRS, ASR90 & XR120 series)
* **IOS-XE**: Service Provider routers (ASR10)


* SSH/Telnet to management IP Address of a Cisco devices allows for simple configuration.
* For enterprise networks, secure login will be initiated with a centralised AAA server:
	* **A**uthentication
	* **A**uthorization
	* **A**ccounting


##### CLI Modes:
`hostname>`User Exec Mode<br>
`hostname#`Privilage Exec Mode

##### Tree Structure of User Modes
```
User EXEC

Privileged EXEC
├── Gloal
│   ├── Interface
│   ├── Config-vlan
│   └── Line
└── VLAN
```
![img](Resources/CLI%20Modes.png)

Tips:
* `?` Shows available command
* **Ambigious command**: incomplete suggestion
* A `?` right after a set of characters gives possible commands that could be auto-completed with.
* A `<space>` followed by `?` right after the right after a set of characters gives possible commands that could be tagged along with the current command.
* When browsing help,`spacebar` shows the next page of information; `Ctrl + C` shows the entire list.
* `tab` auto-completes the command.
* Sometimes the **Invalid input** error could also be due the the wrong access level.
* Up arrow cycles through previously entered command.
* `Ctrl + W` is the backspace
* `show` & `debug` commands are enterd in Privileged EXEC mode.
* `do` followed by a Privileged EXEC mode command executes the command in Privileged EXEC mode while in other mode.

## Transport Layer Header
* Provides transparent transfer of data among hosts
* Flow Control: Adjusting flow of data from tx to ensure rx can handle the size.
* Session Multiplexing: Support of multiple session and management of indiv. traffic over single link.
* Most common Layer-4 protocols:
	* TCP [Transport Control Protocol]
	* UDP [User Datagram Protocol]

### TCP
* Connection Oriented
* Once establish, communication can take place
	* Meaning, data can be sent and be read at both ends.
* Encorporates "Sequencing" to maintain order and accountability.
* Reliable.
* Performs Flow Control.

```
Sender				Receiver
	├──────────SYN──────>───┤
	├───<────SYN-ACK────────┤
	├──────────ACK──────>───┤
```

#### Components of TCP Header
* Source Port
* Destination Port
* Sequence Number
* Acknowledgement Number
* Header Length
* Reserved
* Code Bits
* Window: Flow Control
* Checksum: Corruption Check
* Urgent
* Options
* Data

### UDP
* Not connection oriented.
* Covid-19 friendly; no handshakes.
* No sequenceing.
* Unreliable
* No Flow Control
* Error Detection & Recovery are done by the upper layers.

#### Components of TCP Header
* Source Port
* Destination Port
* Length
* Checksum
* Data

### IP Header
#### Components of TCP Header
* IP Version (4): IPv4 or IPv6
* Header Length (4): 
* Type of Service
* Total Length (in bytes)
* TTL (8)
* Protocol (8)
* Header Checksum (16)
* Source IP Address (32)
* Destination IP Address (32)
* Header Options (0-40, if any)
* Data (variable length)


## Unicast / Multicast / Broadcast
* Routers do not forward Broadccast Traffic.
* Broadcast traffic is only sent to all the hosts within the network/LAN/Switch.

* Multicast saves bandwidth and sends traffic only those who are interested/subscribed.


## Subnet Addressing
### Excerpt from TCP IP Illustration
#### Classful Addressing
* When the Internet's address structure was originally defined, every unicast IP address had a **network** portion, to identify the network on which the interface using the IP address was to be found, and a **host** portion, used to identify the particular host on host network given in the network portion.<br>
* Thus, some number of contiguous bits in the address became known as the **net number** were used somewhat interchangeably.<br>
* With the realization that different networks might have different numbers of hosts, and that each host requires a unique IP address, a partitioning was devised wherein different-size allocation units of IP address space could be given out to different sites, based on their current and projected number of hosts.<br>
* The partitioning of the address space involved five **classes**.<br>
* Each class represented a different trade-off in the number of bits of a 32-bit IPv4 address devoted to the network number versus the number of bits devoted to the host number.<br>

![img](Resources/Classful%20Addressing.png)












